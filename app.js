const express = require("express");
const config = require("config");
const bodyParser = require("body-parser");
var morgan = require("morgan");
const app = express(express.json());
const usuarios = require('./routes/usuarios.js')
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/usuarios',usuarios);

//Configutación de entornos
console.log("Aplicación: " + config.get("nombre"));
console.log("BD Server: " + config.get("configDB.host"));

// Uso de MIDDLEWARE MORGAN PARA LOG DE PETICIONES
app.use(morgan("tiny"));
console.log("MORGAN HABILITADO...");

app.get("/", function (req, res) {
  res.send("");
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`ESCUCHANDO PUERTO ${port}...`);
});
