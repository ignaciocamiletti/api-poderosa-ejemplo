const express = require("express");
const rutas = express.Router();
const Joi = require("@hapi/joi");

const usuarios = [
    { id: 1, nombre: "Cristobal" },
    { id: 2, nombre: "Gabriel" },
  ];

rutas.get("/", (req, res) => {
  res.send(usuarios);
});

rutas.get("/:id", (req, res) => {
  let usuario = existeUsuario(req.params.id);

  if (!usuario)
    res
      .status(404)
      .send("No se encontro ningún usuario para el id : " + req.params.id);
  res.send(usuario);
});

rutas.post("/addUsuario", (req, res) => {
  const { error, value } = validarUsuario(req.body.nombre);

  if (!error) {
    const usuario = {
      id: usuarios.length + 1,
      nombre: value.nombre,
    };
    usuarios.push(usuario);
    res.send(usuario);
  } else {
    const mensaje = error.details[0].message;
    res.status(400).send(mensaje);
  }
});

rutas.put("/:id", (req, res) => {
  let usuario = existeUsuario(req.params.id);
  if (!usuario) {
    res
      .status(404)
      .send("No se encontro ningún usuario para el id : " + req.params.id);
    return;
  }
  const { error, value } = validarUsuario(req.body.nombre);

  if (error) {
    const mensaje = error.details[0].message;
    res.status(400).send(mensaje);
    return;
  }

  usuario.nombre = value;

  res.send(usuario);
});

rutas.delete("/:id", (req, res) => {
  let usuario = existeUsuario(req.params.id);

  if (!usuario) {
    res.status(400).send("El usuario no fue encontrado");
    return;
  }

  const index = usuarios.indexOf(usuario);
  usuarios.splice(index, 1);
  res.send(usuarios);
});


function existeUsuario(id) {
    return usuarios.find((u) => u.id === parseInt(id));
  }
  
  function validarUsuario(nombre) {
    var schema = Joi.object().keys({
      nombre: Joi.string().min(3).required().messages({
        "string.base": `"nombre" debe ser de tipo string`,
        "string.empty": `"nombre" no puede ser un campo vacío`,
        "string.min": `"nombre" debe tener una longitud mínima de {#limit}`,
        "any.required": `"nombre" es un campo requerido`,
      }),
    });
  
    return schema.validate({ nombre: nombre }, { abortEarly: false });
  }
  

  module.exports = rutas;